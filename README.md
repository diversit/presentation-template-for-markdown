# README

This is template for setting up a project structure for creating presentations using [Markdown](http://daringfireball.net/projects/markdown/).

* The presentation is styled using CSS.
* It's generated into HTML using [Jekyll](http://jekyllrb.com).
* The slides are generated using [RemarkJS](https://remarkjs.com/).
* Via Gulp and BrowserSync it is possible to interactively write the presentation and see the resulting slides in the browser.

# Getting started

* install [Jekyll](http://jekyllrb.com) locally.
* Clone this project.
* Run 'gulp' in the root of the project.  
  This will start [Jekyll](http://jekyllrb.com) in watch-mode and a server with browser-sync and it will open the generated slides in the browser.

This shows a sample presentation.  
The markdown for this presentation is in 'index.html'.  
Jekyll will generate a HTML version in the _site folder using a layout which is defined in the markdown file.  
The layout file is located in the _layout folder.  
It will use a css-style based on the 'conference' and 'year' variables in the markdown file. The css file is located in the 'remarkjs/css' folder.

Additional images can be put in the 'images' folder.

A layout for the **Devoxx 2016** conference is available since I was creating this for a presentation I present there.

### slide ratio
By default the ratio of the slides is set to 16:9.
This can be modified by setting the 'ratio' property in the _layouts/devoxx.html template (somewhere near the bottom) to a different value.

### node_modules
The node_modules folder is checked in on purpose to make this a clone-and-play repo. I have had so many problems in the past with Node/NPM/Gulp etc because of updated or deleted dependencies and I just want to be able to use this everywhere.

# Adding a Youtube video on a slide

To just add a (youtube) video, you can add an _iframe_ to the slide

```javascript
.video[<iframe width="560" height="315" src="https://www.youtube.com/embed/_CQA3X-qNgA?autoplay=0&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>]
```

You can modify the video size to your needs.
If you need additional css styles, e.g. to position the video in the centre of the slide, add a `.video { }` section to the css file and add the needed styles there.

### Full screen video
To make the video the full side of the slide:

* create a completely blank remarkjs template, so without the logo and footer.  
In the _layouts/devoxx.html_ add this to the _textarea id="source"_:
```
name: no-logo
layout: true
---
```
* Use the 'no-logo' template for the slide with the video by adding this just below the slide  
separator '---' so the slide with the video looks like this:
```
---
template: no-logo
.video[<iframe width="560" height="315" src="https://www.youtube.com/embed/_CQA3X-qNgA?autoplay=0&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>]
---
```
* Add this to the css style to make the video full screen.
```css
.video {
}
.video iframe {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}
```

Note: The youtube url can be adjusted with additional options. See this ['YouTube Embedded Players and Player Parameters' page](https://developers.google.com/youtube/player_parameters).  
One of the features it to automatically start the video, but this does not work for slides since the video starts playing when the html page is loaded even though that slide is not shown yet.

# Generating PDF of slides

[Decktape](https://github.com/astefanutti/decktape) is an awesome tool to generate a pdf of your slides.
It even keeps the ratio of you slides!

To generate the pdf using docker:

* run 'gulp' in the root of the project to generate the slides and serve it as a html page in a local http server.
```
./gulp
```
* add an alias for the localhost adapter. Otherwise the docker container cannot access your local http server.  
```
sudo ifconfig lo0 alias 172.16.123.1
```
* run 
```
docker run --rm -v `pwd`:/slides astefanutti/decktape remark http://172.16.123.1:3000 slides.pdf
```


The slides.pdf is now located in the folder in which you ran the docker command.